//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Miguel Vicario on 11/9/18.
//  Copyright © 2018 Miguel Vicario. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        
        let session = Session.sharedInstance
        session.token = nil   //Para que testSaveNil() pase la prueba ya que el test anterior generaba un token
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCorrectLogin(){
        XCTAssertTrue(User.login(userName: "iOSLab", password: "Test"))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "Miguel", password: "Test"))
    }
    
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "223")
        XCTAssertNotNil(session.token)
    }
    
    func testSaveSessionNil(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab1234", password: "223")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken(){
        let _ = User.login(userName: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890","Token Should Match")
    }
    
    func testMusicSongs(){
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fecthSongs{ (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
    
    func testExpectedTokenNotEqual(){
        let _ = User.login(userName: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertNotEqual(session.token!, "123456789","Token Match")
    }
    
    func testfetchSongsThrowError(){
       XCTAssertThrowsError(try User.fetchSongs())
    }
    
}
