//
//  NetworkTests.swift
//  LabTunesTests
//
//  Created by Miguel Vicario on 11/9/18.
//  Copyright © 2018 Miguel Vicario. All rights reserved.
//

import XCTest
@testable import LabTunes

class NetworTests: XCTestCase{
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: .default)
    }
    
    func testValidaCallToItunes(){
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=Queen")
        var statusCode: Int?
        var responseError: Error?
        let promise = expectation(description: "Handler Invoked")
        
        let dataTask = sessionUnderTest.dataTask(with: url!){ data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill()
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
    func testSlowValidaCallToItunes(){
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=Morat")
        let promise = expectation(description: "Status Code: 200")
        
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            if let error = error{
                XCTFail("Error: (\(error.localizedDescription)")
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode{
                if statusCode == 200{
                    promise.fulfill()
                }
                else{
                    XCTFail("Status Code \(statusCode)")
                }
            }
        }
        
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
}


